package de.kotlincook.springular3.controller

import de.kotlincook.springular3.model.ChatMessage
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.handler.annotation.SendTo
import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.stereotype.Controller

@Controller
class ChatController {

    @MessageMapping("/chat.send")
    @SendTo("/topic/public")
    fun sendMessage(@Payload message: ChatMessage): ChatMessage {
        return message
    }

    @MessageMapping("/chat.newUser")
    @SendTo("/topic/public")
    fun newUser(@Payload message: ChatMessage, headerAccessor: SimpMessageHeaderAccessor): ChatMessage {
        headerAccessor.sessionAttributes!!["username"] = message.sender
        return message
    }

}