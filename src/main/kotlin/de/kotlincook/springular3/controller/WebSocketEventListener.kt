package de.kotlincook.springular3.controller

import de.kotlincook.springular3.model.ChatMessage
import de.kotlincook.springular3.model.MessageType
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.SimpMessageSendingOperations
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.messaging.SessionDisconnectEvent

@Component
class WebSocketEventListener {

    private val LOGGER: Logger = LoggerFactory.getLogger(WebSocketEventListener::class.java)

    @Autowired
    lateinit var sendingOperations: SimpMessageSendingOperations


    @EventListener
    fun handleWebSocketConnectListener(event: EventListener) {
        LOGGER.info("Bing, bong, bing. Wir haben eine neue Verbindung!")
    }

    @EventListener
    fun handleWebSocketDisconnectListener(event: SessionDisconnectEvent) {
        val headerAccessor = StompHeaderAccessor.wrap(event.message)
        val username = headerAccessor.sessionAttributes!!["username"] as String
        val chatMessage = ChatMessage(
            type = MessageType.DISCONNECT,
            sender = username,
            content = null,
            time = null)
        sendingOperations.convertAndSend("/topic/public", chatMessage)
    }

}