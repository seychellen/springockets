package de.kotlincook.springular3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Springular3Application

fun main(args: Array<String>) {
    runApplication<Springular3Application>(*args)
}
