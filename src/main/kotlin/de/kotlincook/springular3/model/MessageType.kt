package de.kotlincook.springular3.model

enum class MessageType {
    CHAT, CONNECT, DISCONNECT
}